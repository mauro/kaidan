<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>برنامج XMPP/جابر بسيط وسهل للاستخدام</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>الرخصة:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>عرض الشفرة المصدرية على الويب</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">إلغاء</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation>انسخ الرسالة</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>عدّل الرسالة</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation>تنزيل</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>اكتب رسالة</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>صورة</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>فيديو</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>صوت</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>وثيقة</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>ملف آخر</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>اختر ملفا</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>تعذر حفظ الملف: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>فشل التنزيل: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Food</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>من فضلك اختر محادثة للشروع في المراسلة</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>اختر ملفا</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation>الانتقال إلى المجلد الأصلي</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>أغلق</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation>الخروج</translation>
    </message>
    <message>
        <source>About</source>
        <translation>عن</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>دعوة أصدقاء</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>تم نسخ رابط الدعوة إلى الحافظة</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>لِج</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>لِج إلى حسابك XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>هويتك على جابر:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>user@example.org</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>كلمتك السرية:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>جارٍ الاتصال…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>اتصل</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>اسم المستخدم أو كلمة السر غير صحيحة.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>لا يمكن الاتصال بالخادم. يرجى التحقق من اتصالك بالإنترنت.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>الخادم لا يدعم الاتصالات الآمنة.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>خطأ أثناء محاولة الاتصال الآمن.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>تعذر الإتصال بالخادم.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>حدث خطأ مجهول ؛ اطلع على التفاصيل في السجل.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">متوفر</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">متوفر للدردشة</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">غائب</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">لا تزعجني</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">غائب لفترة قد تطول</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">غير متصل</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">خطأ</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>أضف مُراسلا جديدا</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>معرف جابر:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>user@example.org</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>الاسم المستعار:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>رسالة اختيارية:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>أضف</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">متوفر</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">متوفر للدردشة</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">غائب</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">لا تزعجني</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">غائب لفترة قد تطول</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">غير متصل</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">خطأ</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>جارٍ الاتصال…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>جهات الاتصال</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>أضف مُراسلا جديدا</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>غير متصل</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>احذف المُراسِل</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>حذف</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">إلغاء</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>ارسل</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>طلب اشتراك</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>رفض</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>قبول</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File</source>
        <translation>الملف</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">عن</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
