<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Een eenvoudige, gebruiksvriendelijke Jabber/XMPP-client</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licentie:</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="obsolete">Broncode op GitHub</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Broncode online bekijken</translation>
    </message>
</context>
<context>
    <name>AboutSheet</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="vanished">Een simpele, gebruiksvriendelijke Jabber/XMPP client</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licentie:</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="vanished">Broncode op GitHub</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <source>Navigate Back</source>
        <translation type="vanished">Ga Terug</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Annuleer</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation>Bericht Kopiëren</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Bericht Bewerken</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished">Spoiler</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Downloaden</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Bericht opstellen</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Versturen</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Beeld</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Document</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Ander bestand</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Selecteer een bestand</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Bericht met spoileralert versturen</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation type="unfinished">Spoilerhint</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContextDrawer</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Handelingen</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Kon bestand niet opslaan: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Downloaden mislukt: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Food</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation type="unfinished">Selecteer een chat om een gesprek te beginnen.</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Selecteer een bestand</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation>Ga naar bovenliggende map</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">Sluit</translation>
    </message>
</context>
<context>
    <name>ForwardButton</name>
    <message>
        <source>Navigate Forward</source>
        <translation type="vanished">Ga Vooruit</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Back</source>
        <translation type="vanished">Terug</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>Log uit</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Nodig vrienden uit</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Uitnodigingslink gekopieerd naar klembord</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>De link wordt geopend nadat u verbinding hebt gemaakt.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Log in</translation>
    </message>
    <message>
        <source>Log in to your XMPP Account</source>
        <translation type="vanished">Log in op uw XMPP-account</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>Uw Jabber-ID:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Uw diaspora*-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>gebruiker@voorbeeld.nl</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">gebruiker@diaspora.pod</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>Uw Wachtwoord:</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Opnieuw proberen</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Verbind</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Verbinden…</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Log in op uw XMPP-account</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Ongeldige gebruikersnaam of wachtwoord.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Kan geen verbinding maken met de server. Controleer uw internetverbinding.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>De server ondersteunt geen beveiligde verbindingen.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Fout bij het maken van een beveiligde verbinding.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>Kan het adres van de server niet oplossen. Controleer uw JID opnieuw.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>Kan geen verbinding maken met de server.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Authenticatieprotocol niet ondersteund door de server.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Er is een onbekende fout opgetreden; zie logboek voor details.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Kan geen bericht verzenden, omdat er geen verbinding is gemaakt.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Bericht kon niet verbeterd worden, omdat er geen verbinding is.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished">Spoiler</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Beschikbaar</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Beschikbaar voor gesprek</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Afwezig</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">Niet storen</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Langere tijd afwezig</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Offline</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Fout</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Voeg nieuw contact toe</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Bijnaam:</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>gebruiker@voorbeeld.nl</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Voeg toe</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Dit stuurt ook een verzoek om aanwezigheidsupdates naar het contact.</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Optioneel bericht:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Vertel je gesprekspartner wie je bent.</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Offline</source>
        <translation type="obsolete">Offline</translation>
    </message>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Fout: Controleer nogmaals het JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Beschikbaar</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Beschikbaar voor gesprek</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Afwezig</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Niet storen</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Langere tijd afwezig</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Fout</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Kan geen contact toevoegen omdat er geen verbinding is.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Kan het contact niet verwijderen omdat er geen verbinding is.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished">Spoiler</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Contacts</source>
        <translation>Contacten</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Voeg nieuw contact toe</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Verbinding maken…</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &quot;%1&quot; from your roster?</source>
        <translation type="vanished">Wil je echt contact &quot;%1&quot; uit je contactenlijst verwijderen?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Verwijder contact</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Wil je echt het contact &lt;b&gt;%1&lt;/b&gt; uit je contactenlijst verwijderen?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Annuleer</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Annuleer</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="unfinished">Verstuur</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation>Bijschrift</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation type="unfinished">Abonneringsverzoek</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Je hebt een abonneringsverzoek ontvangen van &lt;b&gt;%1&lt;/b&gt;. Als je toestemt heeft deze account toegang tot je aanwezigheidsupdates.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Afwijzen</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Accepteren</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Kon bestand niet verzenden omdat er geen verbinding is.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Over</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
