<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Un cliente de Jabber/XMPP sencillo y fácil de usar</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licencia:</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="vanished">Código fuente en GitHub</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Vea el código fuente en línea</translation>
    </message>
</context>
<context>
    <name>AboutSheet</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="vanished">Un cliente de Jabber/XMPP sencillo y fácil de usar</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licencia:</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="vanished">Código fuente en GitHub</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <source>Navigate Back</source>
        <translation type="vanished">Navegar hacia atrás</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Cambiar contraseña</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Contraseña actual:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Nueva contraseña:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Nueva contraseña (repetir):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Las nuevas contraseñas no coinciden.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>La contraseña actual no es válida.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Necesita estar conectado para cambiar su contraseña.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Después de cambiar su contraseña, necesitará introducirla en todos sus dispositivos.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Message copied to clipboard</source>
        <translation type="vanished">Mensaje copiado al portapapeles</translation>
    </message>
    <message>
        <source>Copy Message</source>
        <translation>Copiar mensaje</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Editar mensaje</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Crear mensaje</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Enviar</translation>
    </message>
    <message>
        <source>Please choose a file to upload</source>
        <translation type="vanished">Elija un archivo para subir</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Imagen</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Documento</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Otro archivo</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Elegir un archivo</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Enviar un mensaje de «spoiler»</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>Insinuación de «spoiler»</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContextDrawer</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Acciones</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>No se pudo guardar el archivo: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Error de descarga: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <source>People</source>
        <translation>Gente</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Naturaleza</translation>
    </message>
    <message>
        <source>Food</source>
        <translation>Comida</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation>Actividad</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Viajes</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Objetos</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Banderas</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation>Buscar «emoji»</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Seleccione un chat para comenzar a enviar mensajes</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Elegir un archivo</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>go to parent folder</source>
        <translation type="vanished">ir a la carpeta superior</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>Go to parent folder</source>
        <translation>Ir a la carpeta superior</translation>
    </message>
</context>
<context>
    <name>ForwardButton</name>
    <message>
        <source>Navigate Forward</source>
        <translation type="vanished">Navegar hacia adelante</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Back</source>
        <translation type="vanished">Atrás</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>Cerrar sesión</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Invitar amigos</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Copiado el enlace de invitación al portapapeles</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>Could not send message, because not being connected.</source>
        <translation type="vanished">No se pudo enviar el mensaje porque no está conectado.</translation>
    </message>
    <message>
        <source>Could not add contact, because not being connected.</source>
        <translation type="vanished">No se pudo añadir el contacto porque no está conectado.</translation>
    </message>
    <message>
        <source>Could not remove contact, because not being connected.</source>
        <translation type="vanished">No se pudo eliminar el contacto porque no está conectado.</translation>
    </message>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">No se pudo enviar el mensaje debido a que no está conectado.</translation>
    </message>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation type="vanished">No se pudo añadir el contacto debido a que no está conectado.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation type="vanished">No se pudo eliminar el contacto debido a que no está conectado.</translation>
    </message>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>El enlace se abrirá después de que se haya conectado.</translation>
    </message>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation type="vanished">No se pudo enviar el archivo porque no está conectado.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Iniciar sesión</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Inicie sesión en su cuenta XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>Su Jabber-ID:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Su diaspora*-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>usuario@example.org</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">usuario@diaspora.pod</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>Su contraseña:</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Reintentar</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Conectando…</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Usuario o contraseña no válido.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>No se puede conectar al servidor. Compruebe su conexión a internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>El servidor no admite conexiones seguras.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Error al intentar conectar seguramente.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>No se pudo resolver la dirección del servidor. Compruebe su JID de nuevo.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>No se pudo conectar al servidor.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>El protocolo de autenticación no es compatible con el servidor.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Ha ocurrido un error desconocido; vea el registro (log) para detalles.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>No se pudo enviar el mensaje porque no está conectado.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>No se pudo corregir el mensaje porque no está conectado.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>No se pudo enviar el mensaje.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>Falló la corrección del mensaje.</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Disponible</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Libre para chatear</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Ausente</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">No molestar</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Ausente por bastante tiempo</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Desconectado</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>Contraseña cambiada con éxito.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Error al cambiar la contraseña: %1</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Añadir nuevo contacto</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Alias:</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>usuario@example.org</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Esto también enviará una solicitud para acceder a la presencia del contacto.</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Mensaje opcional:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Dígale a su compañero de conversación quien es.</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Error: compruebe el JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Disponible</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Libre para chatear</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Ausente</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">No molestar</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Ausente por bastante tiempo</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Desconectado</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Error</translation>
    </message>
    <message>
        <source>Invalid</source>
        <translation type="vanished">No válido</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>No se pudo añadir el contacto porque no está conectado.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>No se pudo eliminar el contacto porque no está conectado.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Contacts</source>
        <translation>Contactos</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Añadir nuevo contacto</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Conectando…</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &quot;%1&quot; from your roster?</source>
        <translation type="vanished">¿Realmente desea eliminar el contacto &quot;%1&quot; de su lista?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Borrar contacto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;%1&gt; from your roster?</source>
        <translation type="vanished">¿Realmente desea borrar el contacto &lt;%1&gt; de su lista?</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>¿Realmente desea eliminar el contacto &lt;b&gt;%1&lt;/b&gt; de su lista?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Cancelar</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation>Título</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Cambiar contraseña</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Cambia la contraseña de su cuenta. Necesitará introducirla en todos sus dispositivos.</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Solicitud de suscripción</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Ha recibido una solicitud de suscripción de &lt;b&gt;%1&lt;/b&gt;. Si la acepta, la cuenta tendrá acceso a su estado de presencia.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Rechazar</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
</context>
<context>
    <name>ToolBarApplicationHeader</name>
    <message>
        <source>More Actions</source>
        <translation type="vanished">Más acciones</translation>
    </message>
</context>
<context>
    <name>ToolBarPageHeader</name>
    <message>
        <source>More Actions</source>
        <translation type="vanished">Más acciones</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>No se pudo enviar el archivo porque no está conectado.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Acerca de</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
