<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Un client Jabber/XMPP, simple et convivial</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licence :</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="vanished">Code source sur GitHub</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Voir le code source en ligne</translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">À propos</translation>
    </message>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="vanished">Un client Jabber/XMPP, simple et convivial</translation>
    </message>
    <message>
        <source>Sourcecode on GitHub</source>
        <translation type="vanished">Code source sur GitHub</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Fermer</translation>
    </message>
</context>
<context>
    <name>AboutSheet</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation type="vanished">Un client Jabber/XMPP, simple et convivial</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licence :</translation>
    </message>
    <message>
        <source>Source code on GitHub</source>
        <translation type="vanished">Code source sur GitHub</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <source>Navigate Back</source>
        <translation type="vanished">Retour</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Modifier le mot de passe</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Mot de passe actuel :</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Nouveau mot de passe :</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Nouveau mot de passe (répétez) :</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Les nouveaux mots de passe ne sont pas identiques.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Le mot de passe actuel est invalide.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Vous devez être connecté(e) pour modifier votre mot de passe.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Après modification de votre mot de passe, vous devrez le ressaisir sur tous vos autres appareils.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Modifier</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Message copied to clipboard</source>
        <translation type="vanished">Message copié dans le presse-papier</translation>
    </message>
    <message>
        <source>Copy Message</source>
        <translation>Copier le message</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Modifier le message</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Révélation</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Écrire un message</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Envoyer</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Vidéo</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Document</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Autre fichier</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Envoyer un message contenant une révélation</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>Indice sur la révélation</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContextDrawer</name>
    <message>
        <source>Actions</source>
        <translation type="vanished">Actions</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Impossible d&apos;enregistrer le fichier : %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Échec du téléchargement : %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>People</source>
        <translation>Personnes</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Nature</translation>
    </message>
    <message>
        <source>Food</source>
        <translation>Aliments</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation>Activités</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Voyage</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Objets</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Symboles</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Drapeaux</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation>Rechercher un émoji</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Sélectionnez une discussion pour commencer à échanger</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Sélectionnez un fichier</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>go to parent folder</source>
        <translation type="vanished">aller au dossier parent</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Go to parent folder</source>
        <translation>Aller au dossier parent</translation>
    </message>
</context>
<context>
    <name>ForwardButton</name>
    <message>
        <source>Navigate Forward</source>
        <translation type="vanished">Naviguer en avant</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation>Se déconnecter</translation>
    </message>
    <message>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation type="vanished">Ajouter un nouveau contact</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Retour</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Inviter des amis</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Lien d&apos;invitation copié dans le presse-papier</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">Impossible d&apos;envoyer le message car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation type="vanished">Impossible d&apos;ajouter un contact car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation type="vanished">Impossible de supprimer le contact car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Le lien s&apos;ouvrira une fois que vous serez connecté</translation>
    </message>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation type="vanished">Impossible d&apos;envoyer le fichier car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>Votre identifiant Jabber :</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>identifiant@exemple.org</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>Votre mot de passe :</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">Réessayer</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Rejoindre</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Connexion en cours…</translation>
    </message>
    <message>
        <source>Log in</source>
        <translation>Se connecter</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Connexion à votre compte Jabber</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Votre identifiant diaspora* :</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">utilisateur@diaspora.pod</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Identifiant ou mot de passe invalide.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Impossible de se connecter au serveur. Veuillez tester votre connexion à internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Le serveur ne permet pas les connexions sécurisées.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Une erreur est survenue lors de la connexion sécurisée.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>Impossible d&apos;atteindre l&apos;adresse du serveur. Veuillez vérifier votre identifiant Jabber.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>Impossible de se connecter au serveur.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Protocole d&apos;authentification non supporté par le serveur.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Une erreur inconnue est survenue, consultez les journaux pour plus de détails.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Impossible d&apos;envoyer le message car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Impossible de corriger le message car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Révélation</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Le message n&apos;a pas pu être envoyé.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>La correction du message a échoué.</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Disponible</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Libre pour discuter</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Absent(e)</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">Ne pas déranger</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Absent(e) pour longtemps</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished">Hors ligne</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>Le mot de passe a été changé avec succès.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Échec lors de la modification du mot de passe : %1</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Nickname:</source>
        <translation>Pseudonyme :</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Identifiant Jabber :</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>identifiant@exemple.org</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Ajouter un nouveau contact</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Cette action va aussi envoyer une requête pour s&apos;assurer de la présence du contact.</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Message facultatif :</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Informez votre contact sur votre identité.</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Erreur : veuillez vérifier votre identifiant Jabber.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Disponible</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Libre pour discuter</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Absent(e)</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Ne pas déranger</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Absent(e) pour longtemps</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Hors ligne</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Erreur</translation>
    </message>
    <message>
        <source>Invalid</source>
        <translation type="vanished">Invalide</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Impossible d&apos;ajouter un contact car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Impossible de supprimer le contact car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation type="vanished">Cette action va aussi envoyer une requête pour s&apos;assurer de la présence du contact.</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation type="vanished">Message facultatif :</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation type="vanished">Informez votre contact de votre identité.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Révélation</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Contacts</source>
        <translation>Contacts</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Ajouter un nouveau contact</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Connexion en cours…</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &quot;%1&quot; from your roster?</source>
        <translation type="vanished">Voulez-vous vraiment supprimer le contact « %1 » de votre répertoire ?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Supprimer le contact</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Voulez-vous vraiment supprimer le contact &lt;b&gt;%1&lt;/b&gt; de votre répertoire ?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <source>Subscription Request</source>
        <translation type="vanished">Demande d&apos;abonnement</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation type="vanished">Vous avez reçu une demande d&apos;abonnement de la part de &lt;b&gt;%1&lt;/b&gt;. Si vous acceptez, il aura accès à votre statut de présence.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation type="vanished">Refuser</translation>
    </message>
    <message>
        <source>Caption</source>
        <translation>Légende</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Modifier le mot de passe</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Modifie le mot de passe du compte. Vous devrez le ressaisir sur tous vos autres appareils.</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Demande d&apos;abonnement</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Vous avez reçu une demande d&apos;abonnement de la part de &lt;b&gt;%1&lt;/b&gt;. Si vous acceptez, il aura accès à votre statut de présence.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Refuser</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Impossible d&apos;envoyer le fichier car vous n&apos;êtes pas connecté(e).</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation type="vanished">Accepter</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>More Actions</source>
        <translation type="vanished">Plus d&apos;actions</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">À propos</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
