<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Jednoduchý a uživatelsky přívětivý Jabber/XMPP klient</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licence:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Prohlédnout zdrojový kód online</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Změna hesla</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Stávající heslo:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Nové heslo:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Nové heslo (opakovat):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Nová hesla se neshodují.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Stávající heslo je neplatné.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation>Pro změnu hesla je nutné být připojen.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Po změně hesla bude třeba zadat nové heslo na všech vašich ostatních zařízeních.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation>Zkopírovat zprávu</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation>Editovat zprávu</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Stahnout</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation>Vytvořit zprávu</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Obrázek</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation>Další soubor</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Vybrat soubor</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Zaslat «spoiler» zprávu</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation>«Spoiler» tip</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Download failed.</source>
        <translation type="vanished">Stahování selhalo.</translation>
    </message>
    <message>
        <source>Could not save file: %1</source>
        <translation>Soubor nelze uložit: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Chyba při stahování: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation>Oblíbené</translation>
    </message>
    <message>
        <source>People</source>
        <translation>Lidé</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Příroda</translation>
    </message>
    <message>
        <source>Food</source>
        <translation>Jídlo</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation>Činnosti</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation>Cestování</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Předměty</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Symboly</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation>Vlajky</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation>Hledat emoji</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Pro zahájení konverzace vyberte chat</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation>Vybrat soubor</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation>Přesun o adresář výše</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation>Odhlásit se</translation>
    </message>
    <message>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Pozvat přátele</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Zvací odkaz zkopírován do schránky</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Odkaz se  otevře jakmile se připojíte.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Přihlásit se</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Přihlásit se ke svému XMPP účtu</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation>Vaše Jabber-ID:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Vaše diaspora*-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>uživatel@example.org</translation>
    </message>
    <message>
        <source>user@diaspora.pod</source>
        <translation type="vanished">uživatel@diaspora.pod</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation>Vaše heslo:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Připojování…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Připojit</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Neplatný uživatel nebo heslo.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Nelze se připojit k serveru.
Prosím zkontrolujte své připojení k internetu.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Server nepodporuje bezpečná připojení.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Hlášení o chybě při pokusu o bezpečné připojení.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation>Neznámá adresa serveru. Zkontrolujte znovu vaše JID.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation>Nebylo možné se připojit k serveru.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Ověřovací protokol není kompatibilní se serverem.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation>Došlo k neznámmé chybě; pro více informací zkontrolujte seznam logů.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation>Nelze odeslat zprávu, nejste připojeni.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation>Zprávu nelze editovat, neboť nejste připojeni.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Zprávu se nepodařilo odeslat.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>Oprava zprávy se nezdařila.</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="unfinished">Dostupný</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="unfinished">Dostupný k chatování</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="unfinished">Nepřítomný</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="unfinished">Nerušit</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="unfinished">Dlouhodobě nepřítomný</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished">Chyba</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation>Změna hesla byla úspěšná.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Změna hesla se nezdařila: %1</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Přidat nový kontakt</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Zároveň odešle požkontakt přístupu k informaci o přítomnosti kontaktu.</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>uživatel@example.org</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Alias:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Volitelná zpráva:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Sdělit chatující proti-straně kdo jste.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Error: Please check the JID.</source>
        <translation>Chyba: zkontrolujte prosím JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Dostupný</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Dostupný k chatování</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Nepřítomný</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Nerušit</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Dlouhodobě nepřítomný</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Nepřipojen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Chyba</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Nelze přidat kontakt, neboť nejste připojeni.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Nelze smazat kontakt, neboť nejste připojeni.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>«Spoiler»</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Připojování…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Kontakty</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Přidat nový kontakt</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Odpojen</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation>Opravdu chcete smazat kontakt &lt;b&gt;%1&lt;/b&gt; z vašeho seznamu?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Smazat kontakt</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Zrušit</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation>Titulek</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Odeslat</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Změna hesla</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Změní heslo k vašemu účtu. Bude třeba ho zadat na všech vašich zařízeních.</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Žádost o přihlášení</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Obdržena žádost o přihlášení od &lt;b&gt;%1&lt;/b&gt;. Pokud přijmete, daný účet bude mít přístup k vašemu statusu.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Odmítnout</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Přijmout</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Soubor nelze odeslat, neboť nejste připojeni.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">O aplikaci</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
